#include <stdio.h>
#include <string.h>
#include "rpmpgp.h"

int main(void)
{
	unsigned char input[1] = {0};
	char *text =
		"2fa3f686df876995167e7c2e5d74c4c7b6e48f8068fe0e44208344d480f7904c"
		"36963e44115fe3eb2a3ac8694c28bcb4f5a0f3276f2e79487d8219057a506e4b";
	char *output = NULL;
	DIGEST_CTX ctx;

	/* Avoid: 'Libgcrypt warning: missing initialization - please fix the application' */
	rpmInitCrypto();

	if ((ctx = rpmDigestInit(PGPHASHALGO_BLAKE2B, 0)) &&
	    !rpmDigestUpdate(ctx, input, sizeof(input)) &&
	    !rpmDigestFinal(ctx, (void **)&output, NULL, 1) &&
	    !strcmp(output, text)) {
		printf("OK\n");
		return 0;
	} else {
		printf("FAILURE\nGot %s\nExpected %s\n", output, text);
		return 1;
	}
}
